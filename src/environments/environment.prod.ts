import {HttpHeaders} from "@angular/common/http";

export const environment = {
  production: true,
  apiBaseUrl: "https://mr-choi-noroff-assignment-api.herokuapp.com/trainers",
  defaultHeaders: new HttpHeaders({
    "Content-Type": "application/json",
    "x-api-key": "9aab3033-647d-4dfc-bfa1-2558e5db09fa"
  }),
  welcomeUrl: "/welcome",
  pokeApiBaseUrl: "https://pokeapi.co/api/v2/pokemon"
};
