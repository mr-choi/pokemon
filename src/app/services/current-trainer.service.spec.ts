import { TestBed } from '@angular/core/testing';

import { CurrentTrainerService } from './current-trainer.service';

describe('CurrentUserService', () => {
  let service: CurrentTrainerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CurrentTrainerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
