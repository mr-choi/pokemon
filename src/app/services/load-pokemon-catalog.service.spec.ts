import { TestBed } from '@angular/core/testing';

import { LoadPokemonCatalogService } from './load-pokemon-catalog.service';

describe('LoadPokemonCatalogService', () => {
  let service: LoadPokemonCatalogService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoadPokemonCatalogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
