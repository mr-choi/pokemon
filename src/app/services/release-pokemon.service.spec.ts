import { TestBed } from '@angular/core/testing';

import { ReleasePokemonService } from './release-pokemon.service';

describe('ReleasePokemonService', () => {
  let service: ReleasePokemonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReleasePokemonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
