import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {concatMap, finalize, from, map, mergeMap, Observable, startWith, toArray, zip} from "rxjs";
import {environment} from "../../environments/environment";
import {Pokemon} from "../models/pokemon.model";
import {CurrentTrainerService} from "./current-trainer.service";
import {PokemonData} from "../models/pokemon-data.model";

/**
 * This services is responsible for getting the urls to the images of the pokemons given a list of pokemon names.
 */
@Injectable({
  providedIn: 'root'
})
export class FetchPokemonCollectionService {

  public fetchPokemonLoading = false;
  public collection : Pokemon[] = [];

  constructor(private http : HttpClient, private currentTrainerService : CurrentTrainerService) { }

  /**
   * Obtain an url to an image of a pokémon
   * @param name the name of the pokemon
   * @returns an observable of the image
   */
  private getPokemonImageUrl (name : string) : Observable<string> {
    return this.http.get<PokemonData>(`${environment.pokeApiBaseUrl}/${name}`)
      .pipe(map((pokemonData) => pokemonData.sprites.other["official-artwork"].front_default))
  }

  /**
   * Utility function that converts an observable stream of pokemon names, into one of pokemon objects.
   * In other words, names with an url to the image (see the Pokemon interface).
   * The PokeAPI will be used to realize this (see getPokemonImageUrl).
   * @param names$ Observable of pokemon names
   * @returns Observable of an array with all pokemon objects
   */
  public namesToPokemonObjects(names$ : Observable<string>) {
    // Important: concatMap, unlike mergeMap, ensures the urls are in the same order as their corresponding names.
    return zip(names$, names$.pipe(concatMap(name => this.getPokemonImageUrl(name))))
      .pipe<Pokemon, Pokemon[]>(
        map(([name, url]) => {
          return {name, url}
        }),
        toArray()
      )
  }

  /**
   * Fetch all the urls to the images of all the pokemons in the current trainer's collection.
   */
  public fetchPokemonImages() {
    this.currentTrainerService.currentTrainerUpdate$
      .pipe(startWith(this.currentTrainerService.currentTrainer))
      .subscribe(trainer => {
      if (!trainer) return; // Cancel if there is no current trainer logged in.
      this.fetchPokemonLoading = true;
      const names$ = from(trainer?.pokemon);
      this.namesToPokemonObjects(names$)
        .pipe(finalize(() => this.fetchPokemonLoading = false))
        .subscribe({
          next: pokemons => this.collection = pokemons,
          error: err => {
            alert(`Failed to get pokemon images. (${err.message})`)
            this.collection = [];
          }
        });
    })
  }
}
