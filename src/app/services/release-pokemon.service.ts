import { Injectable } from '@angular/core';
import {CurrentTrainerService} from "./current-trainer.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Trainer} from "../models/trainer.model";

/**
 * This service is responsible for updating the trainer's data when it releases a pokemon.
 */
@Injectable({
  providedIn: 'root'
})
export class ReleasePokemonService {

  constructor(private currentTrainerService : CurrentTrainerService, private http : HttpClient) { }

  /**
   * Release the specified pokemon and remove it from the current trainer's collection.
   * There must be a trainer logged.
   * @param toRelease The pokemon to be released
   */
  public releasePokemon(toRelease: string | undefined) {
    const trainer = this.currentTrainerService.currentTrainer;
    if (trainer) {
      const newCollection = trainer.pokemon.filter(name => toRelease !== name);
      // A patch request will be performed to update the collection of the current trainer.
      this.http.patch<Trainer>(`${environment.apiBaseUrl}/${trainer.id}`,
        {pokemon: newCollection},
        {headers: environment.defaultHeaders}
      ).subscribe({
        next: trainer => this.currentTrainerService.currentTrainer = trainer,
        error: err => alert(`Failed to release ${toRelease}. (${err.message})`)
      })
    }
  }
}
