import { TestBed } from '@angular/core/testing';

import { FetchPokemonCollectionService } from './fetch-pokemon-collection.service';

describe('PokemonService', () => {
  let service: FetchPokemonCollectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FetchPokemonCollectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
