import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Trainer} from "../models/trainer.model";
import {environment} from "../../environments/environment";
import {finalize, map, mergeMap, Observable, share, tap} from "rxjs";
import {Router} from "@angular/router";

/**
 * This service is responsible for managing the current trainer logged in.
 */
@Injectable({
  providedIn: 'root'
})
export class CurrentTrainerService {

  private readonly CURRENT_TRAINER_ID = "currentTrainerId";

  private update = () => {};
  /**
   * An Observable that emits the new trainer object every there it changes (including its information).
   */
  public currentTrainerUpdate$ = new Observable<Trainer|undefined>(
    subscriber => {
      this.update = () => subscriber.next(this._currentTrainer);
    }
  ).pipe(share()); // The share ensures multiple sources can listen to it (enables multicasting).

  private _currentTrainer?: Trainer;

  public get currentTrainer() {
    return this._currentTrainer;
  }
  public set currentTrainer(trainer) {
    this._currentTrainer = trainer;
    this.update();
  }

  /**
   * Set up a get request to the api
   * @param id The id of the requested trainer
   * @returns An observable of the trainer
   */
  private getTrainerById(id: number): Observable<Trainer> {
    return this.http.get<Trainer>(`${environment.apiBaseUrl}/${id}`);
  }
  /**
   * Retrieve the id of the current trainer from the local storage.
   * @returns the id or null if there is no trainer logged in.
   */
  public getIdFromLocalStorage() {
    return localStorage.getItem(this.CURRENT_TRAINER_ID);
  }

  constructor(private http: HttpClient, private router: Router) {
    // Check localStorage. Load the trainer if there is an id stored.
    const id = this.getIdFromLocalStorage();
    if (id) {
      this.loadTrainer(this.getTrainerById(parseInt(id)));
    }
  }

  public trainerLoading = false;
  /**
   * Load the specified trainer and set it as current.
   * If no trainer is specified, the current trainer will be removed.
   * @param trainer$ an Observable of the trainer that should be loaded.
   */
  public loadTrainer(trainer$ : Observable<Trainer>) {
    this.trainerLoading = true;
    trainer$
      .pipe(finalize(() => this.trainerLoading = false))
      .subscribe({
        next: trainer => this.currentTrainer = trainer,
        error: (err: HttpErrorResponse) => {
          alert(`Failed to load trainer data. Logging out. (${err.message})`);
          this.removeTrainer();
        }
      })
  }

  /**
   * End the current trainer session and redirect back to the landing page.
   */
  public removeTrainer() {
    this.currentTrainer = undefined;
    localStorage.removeItem(this.CURRENT_TRAINER_ID);
    this.router.navigateByUrl(environment.welcomeUrl);
  }

  /**
   * Set a new trainer as the current trainer and add it to the API.
   * If it already exists, only set it as the current trainer.
   * @param username The username of the trainer.
   */
  public setNewCurrentTrainer(username: string) {
    // First step is to get request all existing trainers from the API:
    const result$ = this.http.get<Trainer[]>(environment.apiBaseUrl)
      .pipe<Trainer | undefined, Trainer, Trainer>(
        // Then we check in the resulting list of trainers if the entered username already exists:
        map(trainers => trainers.find(trainer => trainer.username === username)),

        // Then we request the data of the trainer if it already exists, or create a new one:
        mergeMap(existingTrainer => !existingTrainer
          ? this.http.post<Trainer>(environment.apiBaseUrl,
            {username, pokemon: []},
            {headers: environment.defaultHeaders})
          : this.getTrainerById(existingTrainer.id)
        ),
        // When done save new trainer id in local storage and navigate to the catalog page:
        tap(trainer => {
          localStorage.setItem(this.CURRENT_TRAINER_ID, trainer.id.toString());
          this.router.navigateByUrl("/");
        })
      );
    this.loadTrainer(result$);
  }
}
