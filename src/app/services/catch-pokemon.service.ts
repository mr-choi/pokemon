import { Injectable } from '@angular/core';
import {Trainer} from "../models/trainer.model";
import {environment} from "../../environments/environment";
import {CurrentTrainerService} from "./current-trainer.service";
import {HttpClient} from "@angular/common/http";
import {finalize} from "rxjs";

/**
 * This service is responsible for updating the trainer's data when it catches pokemon.
 */
@Injectable({
  providedIn: 'root'
})
export class CatchPokemonService {

  public loading = false;

  constructor(private currentTrainerService : CurrentTrainerService, private http : HttpClient) { }

  /**
   * Catch the specified pokemon and add it to the collection of the current trainer.
   * There must be a trainer logged in and the pokemon should not be already caught by the current trainer.
   * @param toCatch The pokemon to catch
   */
  public catchPokemon(toCatch: string | undefined) {
    const trainer = this.currentTrainerService.currentTrainer;
    if (trainer && !trainer.pokemon.find(pokemon => pokemon === toCatch)) {
      const newCollection = [...trainer.pokemon, toCatch];
      this.loading = true;
      // A patch request will be performed to update
      this.http.patch<Trainer>(`${environment.apiBaseUrl}/${trainer.id}`,
        {pokemon: newCollection},
        {headers: environment.defaultHeaders}
      ).pipe(finalize(() => this.loading = false)).subscribe({
        next: trainer => this.currentTrainerService.currentTrainer = trainer,
        error: err => alert(`Failed to catch ${toCatch}. (${err.message})`)
      })
    }
  }
}
