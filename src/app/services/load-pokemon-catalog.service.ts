import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Pokemon} from "../models/pokemon.model";
import {environment} from "../../environments/environment";
import {concatMap, finalize, from} from "rxjs";
import {PokemonResults} from "../models/pokemon-results.model";
import {FetchPokemonCollectionService} from "./fetch-pokemon-collection.service";

/**
 * This service is responsible for loading the pokemon catalog via the pokeAPI.
 */
@Injectable({
  providedIn: 'root'
})
export class LoadPokemonCatalogService {

  constructor(private http : HttpClient, private fetchPokemonService : FetchPokemonCollectionService) { }

  // State variables of the search browser:
  public loading = false;
  public results : Pokemon[] = []
  public offset = 0;
  public limit = 20;
  public maxOffset = 0;

  /**
   * Load the pokemon catalog.
   * @param offset Offset passed to the pokeAPI from where to look in the catalog.
   * @param limit Maximum number of pokemon to be loaded from the catalog.
   */
  public loadPokemonCatalog(offset : number, limit : number) {
    this.offset = offset;
    this.limit = limit;

    // If the given offset and number are already known, then load it from the session storage:
    const stored = sessionStorage.getItem(`${offset},${limit}`);
    if (stored) {
      const {pokemons, maxOffset} = JSON.parse(stored);
      this.results = pokemons;
      this.maxOffset = maxOffset;
      return;
    }

    // Otherwise, we need to consult the api.
    this.loading = true;
    const names$ = this.http.get<PokemonResults>(`${environment.pokeApiBaseUrl}?offset=${offset}&limit=${limit}`)
      .pipe(concatMap(res => {
          this.maxOffset = (Math.ceil(res.count / limit)-1)*limit;
          return from(res.results.map(obj => obj.name));
      }));
    // We need to find the corresponding image url. The fetch pokemon collection service is needed.
    this.fetchPokemonService.namesToPokemonObjects(names$)
      .pipe(finalize(() => this.loading = false))
      .subscribe({
        next: pokemons => {
          this.results = pokemons;
          // Storage session will be used to prevent reloading the same result.
          sessionStorage.setItem(`${offset},${limit}`, JSON.stringify({
            pokemons,
            maxOffset: this.maxOffset
          }));
        },
        error: err => {
          alert(`Failed to load results. (${err.message})`)
          this.results = [];
        }
      })
  }
}
