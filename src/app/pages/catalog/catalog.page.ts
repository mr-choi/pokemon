import { Component, OnInit } from '@angular/core';
import {ButtonAction} from "../../models/buttonAction";
import {LoadPokemonCatalogService} from "../../services/load-pokemon-catalog.service";

/**
 * This page displays a catalog of all available pokemon that can be caught.
 */
@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.page.html',
  styleUrls: ['./catalog.page.css']
})
export class CatalogPage implements OnInit {

  buttonAction = ButtonAction.CATCH;

  constructor(public pokemonCatalogService : LoadPokemonCatalogService) { }

  ngOnInit(): void {
    this.pokemonCatalogService.loadPokemonCatalog(this.pokemonCatalogService.offset, this.pokemonCatalogService.limit);
  }

}
