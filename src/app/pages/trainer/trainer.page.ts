import { Component, OnInit } from '@angular/core';
import {ButtonAction} from "../../models/buttonAction";
import {CurrentTrainerService} from "../../services/current-trainer.service";
import {FetchPokemonCollectionService} from "../../services/fetch-pokemon-collection.service";

/**
 * This page displays the profile of the current trainer: i.e. the pokemon it has caught.
 */
@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {

  buttonAction = ButtonAction.RELEASE;

  constructor(public currentTrainerService : CurrentTrainerService,
              public fetchPokemonService : FetchPokemonCollectionService) {
  }

  /**
   * Event that ends the current trainers session: this leads to redicection to the home page.
   */
  onLogout() {
    this.currentTrainerService.removeTrainer();
  }

  ngOnInit(): void {
    this.fetchPokemonService.fetchPokemonImages();
  }


}
