import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

/**
 * A simple not found page.
 */
@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.page.html',
  styleUrls: ['./not-found.page.css']
})
export class NotFoundPage implements OnInit {

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

}
