import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {CurrentTrainerService} from "../../services/current-trainer.service";

/**
 * The landing page is the page that will be displayed if there is no current trainer logged in.
 */
@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.css']
})
export class LandingPage implements OnInit {

  /**
   * Whether an error must be displayed because the specified username of the trainer is not valid.
   */
  inputError = false;

  constructor(public currentTrainerService : CurrentTrainerService) { }

  /**
   * Submit the given username of the new/existing trainer.
   * It first checks if the given username is valid: at least 3 characters after trimming away the spaces at the ends.
   * @param form The submitted form.
   */
  onSubmitTrainerName(form : NgForm) {
    this.inputError = false;
    const input = form.value.trainerName.trim();
    if (input.length < 3) {
      this.inputError = true;
    } else {
      this.currentTrainerService.setNewCurrentTrainer(input);
    }
  }

  /**
   * Event function that removes the error message when the user starts typing in the input field.
   */
  onInputChange() {
    this.inputError = false;
  }

  ngOnInit(): void {
  }

}
