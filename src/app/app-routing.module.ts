import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CatalogPage} from "./pages/catalog/catalog.page";
import {LandingPage} from "./pages/landing/landing.page";
import {TrainerPage} from "./pages/trainer/trainer.page";
import {NotFoundPage} from "./pages/not-found/not-found.page";
import {TrainerGuard} from "./guards/trainer.guard";

const routes: Routes = [
  {
    path: "catalog",
    component: CatalogPage,
    canActivate: [TrainerGuard]
  },
  {
    path: "trainer",
    component: TrainerPage,
    canActivate: [TrainerGuard]
  },
  {
    path: "welcome",
    component: LandingPage
  },
  {
    path: "",
    pathMatch: "full",
    redirectTo:"/catalog"
  },
  {
    path:"**",
    component: NotFoundPage
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
