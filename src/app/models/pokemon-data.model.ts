export interface PokemonData {
  sprites: {
    other: {
      "official-artwork": {
        front_default : string;
      }
    }
  }
}
