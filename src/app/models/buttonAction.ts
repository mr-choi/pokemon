export enum ButtonAction {
  CATCH = "CATCH",
  RELEASE = "RELEASE",
  NONE = "NONE"
}
