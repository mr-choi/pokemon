export interface PokemonResults {
  count: number;
  next: string | null
  previous: string | null
  results: {name: string}[]
}
