import {Component, Input, OnInit} from '@angular/core';
import {Pokemon} from "../../models/pokemon.model";
import {ReleasePokemonService} from "../../services/release-pokemon.service";

/**
 * Component that is responsible for the release buttons of every pokemon displayed on the trainer page.
 */
@Component({
  selector: 'app-release-button',
  templateUrl: './release-button.component.html',
  styleUrls: ['./release-button.component.css']
})
export class ReleaseButtonComponent implements OnInit {

  @Input()
  pokemon? : Pokemon;

  constructor(public releasePokemonService : ReleasePokemonService) { }

  ngOnInit(): void {
  }

  /**
   * Event handle that releases the pokemon and thus removes from the current trainer's collection.
   * A confirmation of this action will be requested from the user.
   */
  onRelease() {
    if (window.confirm(`Are you sure you want to release ${this.pokemon?.name}?`)) {
      this.releasePokemonService.releasePokemon(this.pokemon?.name);
    }
  }
}
