import {Component, Input, OnInit} from '@angular/core';
import {Pokemon} from "../../models/pokemon.model";
import {ButtonAction} from "../../models/buttonAction";

/**
 * Component that is responsible for listing for both the catalog and the trainer page.
 */
@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css']
})
export class PokemonComponent implements OnInit {

  @Input()
  pokemons : Pokemon[] = [];

  @Input()
  action : ButtonAction = ButtonAction.NONE;
  ButtonAction = ButtonAction;

  constructor() { }

  ngOnInit(): void {

  }

}
