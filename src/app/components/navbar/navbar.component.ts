import { Component, OnInit } from '@angular/core';
import {CurrentTrainerService} from "../../services/current-trainer.service";

/**
 * Simple navigation bar component at top op page. It displays the current trainer.
 */
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public currentTrainerService : CurrentTrainerService) { }

  ngOnInit(): void {
  }

}
