import { Component, OnInit } from '@angular/core';
import {LoadPokemonCatalogService} from "../../services/load-pokemon-catalog.service";
import {Router} from "@angular/router";

/**
 * This component is responsible for the navigation controls at the bottom of the pokemon catalog page.
 */
@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  constructor(public pokeCatService : LoadPokemonCatalogService, public router : Router) { }

  ngOnInit(): void {
  }

  /**
   * Event handle that redirects to the first page of the catalog.
   */
  onFirst() {
    this.pokeCatService.loadPokemonCatalog(0, this.pokeCatService.limit)
  }

  /**
   * Event handle that redirects to the previous page with respect to the current page.
   */
  onPrev() {
    if (this.pokeCatService.offset >= this.pokeCatService.limit) {
      this.pokeCatService.loadPokemonCatalog(
        this.pokeCatService.offset - this.pokeCatService.limit,
        this.pokeCatService.limit
      )
    }
  }

  /**
   * Event handle that redirects to the next page with respect to the current page.
   */
  onNext() {
    if (this.pokeCatService.offset < this.pokeCatService.maxOffset) {
      this.pokeCatService.loadPokemonCatalog(
        this.pokeCatService.offset + this.pokeCatService.limit,
        this.pokeCatService.limit
      )
    }
  }

  /**
   * Event handle that redirects to the last page of the catalog.
   */
  onLast() {
    this.pokeCatService.loadPokemonCatalog(this.pokeCatService.maxOffset, this.pokeCatService.limit)
  }

  /**
   * Event handle that triggers when user changes the maximum number of pokemon on the page.
   * @param event event object containing the selected value.
   */
  onSelect(event: any) {
    this.pokeCatService.loadPokemonCatalog(0, parseInt(event.target.value));
  }
}
