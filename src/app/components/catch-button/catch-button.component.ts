import {Component, Input, OnInit} from '@angular/core';
import {Pokemon} from "../../models/pokemon.model";
import {CatchPokemonService} from "../../services/catch-pokemon.service";
import {CurrentTrainerService} from "../../services/current-trainer.service";
import {startWith} from "rxjs";

/**
 * The catch buttons that are present at every pokemon in the catalog.
 */
@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.css']
})
export class CatchButtonComponent implements OnInit {

  @Input()
  pokemon? : Pokemon;

  enabled = false;

  constructor(public catchPokemonService : CatchPokemonService,
              public currentTrainerService : CurrentTrainerService) { }

  /**
   * Ensures that the catch buttons are disabled if the current trainers has already caught the pokemon.
   */
  ngOnInit(): void {
    this.currentTrainerService.currentTrainerUpdate$
      .pipe(startWith(this.currentTrainerService.currentTrainer))
      .subscribe(trainer => {
        this.enabled = (trainer) ? !trainer.pokemon.find(pokemon => this.pokemon?.name === pokemon) : false;
      })
  }

  /**
   * Event when user catches the pokemon.
   */
  onCatch() {
    this.catchPokemonService.catchPokemon(this.pokemon?.name);
  }
}
