import { Injectable } from '@angular/core';
import {CanActivate, Router, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {CurrentTrainerService} from "../services/current-trainer.service";
import {environment} from "../../environments/environment";

/**
 * Guard to prevent unauthorized access to trainer and catalog page.
 */
@Injectable({
  providedIn: 'root'
})
export class TrainerGuard implements CanActivate {

  constructor(private currentTrainerService : CurrentTrainerService,
              private router : Router) {
  }

  private readonly WELCOME_URL_TREE : UrlTree = this.router.parseUrl(environment.welcomeUrl);

  /**
   * Only if there is a trainer id stored in the local storage, the user may access the catalog and trainer page.
   * Important: this does not guarantee that all the user data is loaded or if the user still exists in the API.
   */
  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.currentTrainerService.getIdFromLocalStorage() ? true : this.WELCOME_URL_TREE;
  }
}
