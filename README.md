# Pokemon

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.3. It is part of a required assignment for the full stack Java developer course at Noroff academy. This assignment focuses on using the Angular framework to build front-end applications.

## Installation
The application can be found via [the following link](https://mr-choi-pokemon-catalog.herokuapp.com).

Installation of the development version requires git and nodejs.
1. Fork and clone this repository.
2. Install dependencies with `npm install`
3. To run the development version, run `ng serve`.

**Important:** 
- The application makes use of 2 API's. One is the [PokéAPI](https://pokeapi.co/) what is used to retrieve Pokémon data. The other one is [a special API required for the assignment](https://github.com/dewald-els/noroff-assignment-api) that we need to deploy ourselves. The latter requires an API key for post and patch requests and this can be set in the `environment.ts` file.
- If you want to deploy this application. Be aware that the `environment.prod.ts` file will be used instead of `environment.ts`.

## Usage
- To use this app, it is required to enter a username. The username must have at least 3 characters.
- Click on PokéCatalog left in the navigation bar to go to the catalog. Here, you can browse for all the Pokémon available. Press the catch button below the Pokémon to catch it and add it to your collection.
- Click on your username right in the navigation bar to go to the trainer page. Here you can view your Pokémon collection. Press the release button below the Pokémon to remove it from your collection.
- On the trainer page, you can also log out with the button below. Your pokemon collection will be saved.

## Credits
Created by Kevin Choi. Pokémon and Pokémon character names are trademarks of Nintendo.
